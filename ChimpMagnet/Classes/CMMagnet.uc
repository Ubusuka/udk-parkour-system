class CMMagnet extends Actor placeable;

enum ECMMagneticChargeT
{

	CMCharge_Negative,
	CMCharge_Neutral ,
	CMCharge_Positive 
	
};

var vector tmpMoveDir;

//var(Custom) StaticMeshComponent visualCue;
var DynamicLightEnvironmentComponent LightEnvironment;

var	CylinderComponent		CylinderComponent;

//Magnetic variables
var Actor tracedActors[6];
var CMMagnet affector;
var(MagnetProperties) ECMMagneticChargeT theCharge;
var(MagnetProperties) bool bLeftSide,bRightSide,bFrontSide,bBackSide,bTopSide,bBottomSide;
var vector trc_left,trc_right,trc_up,trc_down,trc_forward,trc_backward;
var LinearColor chargeColours[3];

var float smallestDistance,currentSmallestDistance;

var MaterialInstanceConstant MatInst[6];
var int materialIndex;

//can this box be dragged towards another? Or is it rooted to the ground
var bool bCanBeMoved;

var TraceHitInfo hitInfo;

var(Sides) CMMagnetModularSide boxSides[6];
var(collisionBounds) float cylinderRadius,cylinderHeight;

var bool bApplyMagnetForce;
var vector forceDirection;
var(Sides) array<CMMagnetModularSide> modularPieces;

event PreBeginPlay()
{
	
	local vector newPos;
	super.PreBeginPlay();

	tmpMoveDir.Z=1;
	//SetPhysics(PHYS_Flying);
	//newPos.Z = CylinderComponent.CollisionHeight;
	CylinderComponent.SetTranslation(newPos);
	SetCollisionType(ECollisionType.COLLIDE_BlockAll);
	
}
event PostBeginPlay()
{


	local int a;

	super.PostBeginPlay();
	InitChargeColours();
	InitTrcDirections();
	a=6;

	for(a=0;a<modularPieces.Length;a++)
	{
		if(modularPieces[a]!=None)
		{
			modularPieces[a].SetBase(self);
			//modularPieces[a].bHardAttach=true;
		}
	}
	//a = defaultPrefab.PrefabArchetypes.Length;

	//for(a=0;a < defaultPrefab.PrefabArchetypes.Length;a++)
	//{
	//	addStaticMesh(StaticMeshActor(defaultPrefab.PrefabArchetypes[a]),a);
	//}

	
}
function changeCharge(int armsCharge)
{
	
	local LinearColor color;
	local int a;
	`log("Changing Charge");
	color.A=0;
	color.R=1;
	color.G=0;
	color.B=0;

	if(armsCharge==1)
	{
		affector=None;
	}
	theCharge = ECMMagneticChargeT(armsCharge);
	for(a=0;a<materialIndex;a++)
	{
		if(MatInst[a] != None)
		 MatInst[a].SetVectorParameterValue('AmbientColor',chargeColours[int(theCharge)]);
	}
	//Playing sound - Paul
	 //SlideSound.Play();
}

function InitTrcDirections()
{
	
	trc_up.Z = 1;
	trc_up.Y=0;
	trc_up.X=0;

	trc_down.Z = -1;
	trc_down.X=0;
	trc_down.Y=0;


	trc_right.X = 1;
	trc_right.Y=0;
	trc_right.Z=0;

	trc_left.X = -1;
	trc_left.Y=0;
	trc_left.Z=0;

	trc_forward.Y = 1;
	trc_forward.X=0;
	trc_forward.Z=0;

	trc_backward.Y = -1;
	trc_backward.Z=0;
	trc_backward.X=0;
}
function InitChargeColours()
{
	chargeColours[0].A=0;
	chargeColours[0].R=1;
	chargeColours[0].G=0;
	chargeColours[0].B=0;

	chargeColours[1].A=0;
	chargeColours[1].R=0;
	chargeColours[1].G=0;
	chargeColours[1].B=1;

	chargeColours[2].A=0;
	chargeColours[2].R=0;
	chargeColours[2].G=1;
	chargeColours[2].B=0;
}
function addStaticMesh(StaticMeshActor TMesh,int iteration)
{
	local StaticMeshComponent SMC;

	//SMA = Spawn(SMA.Class,self,,,,SMA);
	SMC = new(self) class'StaticMeshComponent';
	AttachComponent(SMC);
	
	//`log("Magnet Mesh added "@TMesh.GetDebugName());
	SMC.SetStaticMesh(TMesh.StaticMeshComponent.StaticMesh);
	//SMA.SetLocation(TMesh.Location);
	SMC.SetTranslation(TMesh.Location);
	SMC.SetRotation(TMesh.Rotation);
	SMC.SetScale3D(TMesh.DrawScale3D);
	SMC.SetScale(TMesh.DrawScale);
	SMC.SetLightEnvironment(LightEnvironment);
	SMC.SetActorCollision(true,true,true);

	//`log("Package name: "@SMC.GetPackageName());
	if(SMC.StaticMesh == StaticMesh'SLFX.Meshes.Magnet_Sphere')
	{

		//`log("FOUND 1 SPHERE");
		MatInst[materialIndex] = new(None) Class'MaterialInstanceConstant';
   MatInst[materialIndex].SetParent(SMC.GetMaterial(0));
  
   SMC.SetMaterial(0, MatInst[materialIndex]);
   UpdateMaterialInstance(materialIndex);
   materialIndex++;
		
	}
	//SMC.SetActorCollision(true,true,true);//=true;
	//SMC.SetBlockRigidBody(true);
	//SMC.BlockZeroExtent=true;
	//SMC.BlockNonZeroExtent=true;
	//SMC.CollideActors=true;
	//SMC.HiddenEditor=false;
	//SMC.HiddenGame=false;
	//BlockNonZeroExtent=true
	//	BlockZeroExtent=true
	//	BlockActors=true
	//	CollideActors=true
	//	HiddenEditor=false
	//	HiddenGame=false
}
function UpdateMaterialInstance(int index)
{
	local LinearColor color;
	color.A=0;
	color.R=1;
	color.G=0;
	color.B=0;
   MatInst[index].SetVectorParameterValue('AmbientColor',color);
}
simulated function DisplayDebug(HUD Hud,out float out_YL, out float out_YPos)
{
	local String T;
	local Canvas canvas;
	local int a;

	canvas = Hud.Canvas;

	out_YPos += out_YL;
	Canvas.SetPos(4, out_YPos);
	Canvas.SetDrawColor(255,0,0);
	Canvas.DrawText("MAGNET VELOCITY "@Velocity);
	 //for(a=0;a<defaultPrefab.PrefabArchetypes.Length;a++)
	 //{

		//out_YPos += out_YL;
		//Canvas.SetPos(4, out_YPos);
  //  Canvas.SetDrawColor(255,0,0);
	 //Canvas.DrawText("Name magnet Part "@defaultPrefab.PrefabArchetypes[a].Name, FALSE);

	 //}
   

}
function Stop()
{
	tmpMoveDir.Z=0;
}

event Bump(Actor other,PrimitiveComponent otherComp,vector hitNorm)
{
	//`log("Bumped somethong");

	//bApplyMagnetForce=false;
	//forceDirection.Z=0;
	//forceDirection.X=0;
	//forceDirection.Y=0;
}
//event Touch(Actor other, PrimitiveComponent otherComp,Vector hitloc,Vector hitNorm)
//{
//		`log("Touched somethong");

//	bApplyMagnetForce=false;
//	forceDirection.Z=0;
//	forceDirection.X=0;
//	forceDirection.Y=0;

//}
event Tick(float dTime)
{
	local vector HitNormal,HitLocation;
	local float tempDistance;
	local vector moveDirection;
	local vector localPosition;
	local vector tempUp;
	local float distance;
	local int i;

	tempUp.Z =1;// 100;
	
	localPosition = Location + tempUp;
	affector = None;

	if(bApplyMagnetForce==true)
	{
		//for(i=0;i<modularPieces.Length;i++)
		//{
		//	modularPieces[i].Move(forceDirection*128*dTime);
		//}
		MoveSmooth(forceDirection*128*dTime);
		//SetLocation(Location+forceDirection*128*dTime);
		//Velocity+=forceDirection*128*dTime;
		//Velocity = forceDirection*128*dTime;
		//SetLocation(Location+forceDirection*128*dTime);
		//CollisionComponent.AddForce(forceDirection);
		
	}

	//Send traces in each of the 6 faces of the "magnet cube" used for determining if there is an affector in the way depending on which side can be magnetised
	//if(bLeftSide)
	//{
		//tracedActors[0] = Trace(HitLocation, HitNormal,localPosition + trc_left*32768,Location + tempUp, true,,hitInfo);
		//if(tracedActors[0] != None)
		//{

		//}
	//}
	//if(bRightSide)
	//{
	//	tracedActors[1] = Trace(HitLocation, HitNormal,localPosition + trc_right*32768,Location + tempUp, true);
	//}
	//if(bTopSide)
	//{
	//	tracedActors[2] = Trace(HitLocation, HitNormal,localPosition + trc_up*32768,Location + tempUp, true);
	//}
	//if(bBottomSide)
	//{
	//	tracedActors[3] = Trace(HitLocation, HitNormal,localPosition + trc_down*32768,Location + tempUp, true);
	//}
	//if(bFrontSide)
	//{
	//	tracedActors[4] = Trace(HitLocation, HitNormal,localPosition + trc_forward*32768,Location + tempUp, true);
	//}
	//if(bBackSide)
	//{
	//	tracedActors[5] = Trace(HitLocation, HitNormal,localPosition + trc_backward*32768,Location + tempUp, true);
	//}
	
	//initialise smallestDistance to a ridiculously high number so the first currentSmallestDistance check will be < less than it
	//smallestDistance = 100000000;

	////If the charge on this box is not neutral, we should carry on to see if anything will affect it.
	//if(theCharge!= CMCharge_Neutral)
	//{
	//	for(i=0;i<6;i++)
	//	{
	//		if(tracedActors[i] != None)
	//		{
				
	//			`log("Traced Something "@tracedActors[i].GetDebugName());
	//			//Make sure what we traced is a magnet
	//			if(tracedActors[i].isA('CMMagnet'))
	//			{
				
	//				`log("Traced another magnet");
	//				//If the traced box is neutral, we dont want to know about it, just continue to the next iteration of the loop
	//				if(CMMagnet(tracedActors[i]).theCharge == CMCharge_Neutral)
	//				{
	//					continue;
	//				}
	//				else
	//				{
	//					//if(bumpedMagnet!=None)
	//					//{
	//					//	//if the current traced magnet is the same magnet as we already bumped into and they still have oppposite charges, we dont want to do anything in that direction
	//					//	if(bumpedMagnet == CMMagneticBox(tracedActors[i]) && bumpedMagnet.theCharge != CMMagneticBox(tracedActors[i]).theCharge)
	//					//	{
	//					//		//bumpedMagnet=None;
	//					//		continue;
	//					//	}
	//					//	else
	//					//	{
	//					//		bumpedMagnet=None;
	//					//	}
	//					//}


	//					//currentSmallestDistance is the smallest distance between any magnets in the 6 directions
	//					//We keep looping through each of the directions only storing a new magnet affector if the distance is less than the previous minimum
	//					currentSmallestDistance = VSize(CMMagnet(tracedActors[i]).Location - Location);
	//					if(currentSmallestDistance <= smallestDistance)
	//					{
	//						smallestDistance = currentSmallestDistance;
	//						//if(tracedActors[i].
	//						affector=CMMagnet(tracedActors[i]);
							
	//					}
	//				}
	//			}
	//		}
	//	}
	//}
	//else
	//{
	//	affector=None;
	//	return;
	//}
	
	////If this magnet has a chance to be affected by another
	//if(affector!=None)
	//{

	//	`log("A CHANCE");
	
	//	//store current distance to that affector, used for magnet force
	//	distance=VSize(affector.Location-Location);
	//	tempDistance=distance;
	//	distance = 1.0f/distance;
	//	distance*=600;
	//	//Make sure neither of the magnets charge has changed to neutral during the frame
	//	if(theCharge != CMCharge_Neutral && affector.theCharge != CMCharge_Neutral)
	//	{
			
	//		if(bCanBeMoved)//a boolean that can be set which will be used to determine a static magnet
	//		{
	//			//Obtain the direction to be affected to ( assumes opposite attraction first)
	//		moveDirection = Normal((affector.Location-Location));
			

	//			/*THIS WORKS FINE IF THE ACTOR IS NOT INTERSECTING WITH ANYTHING ELSE AT ALL, MAKE SURE
	//				* THE ENTIRE MESH IS UNOBSTRUCTED*/
	//				if(int(affector.theCharge) != int(theCharge))
	//				{
	//					//Velocity = (moveDirection*128)*(dTime*distance*600);
	//					//Latent move function to move the magnet based on distance
	//					//MoveSmooth((moveDirection*600)*(dTime*distance));
	//					Move((moveDirection*12.8f));
				
	//				}
	//				else
	//				{
	//					//Velocity = (-1*moveDirection*128)*(dTime*distance*600);
	//					//MoveSmooth((-1*moveDirection*600)*(dTime*distance));
	//					Move((-moveDirection*12.8f));
	//				}
			
				
	//		}
			
	//	}

		
	//}
	//	Move(tmpMoveDir*dTime*50);
	
	
}

DefaultProperties
{

	
	materialIndex=0
	theCharge=CMCharge_Neutral

		Begin Object Class=SpriteComponent Name=Sprite
		Sprite=Texture2D'EditorResources.S_Trigger'
		HiddenGame=False
		AlwaysLoadOnClient=False
		AlwaysLoadOnServer=False
		SpriteCategoryName="Triggers"
	End Object
	Components.Add(Sprite)

	Begin Object Class=CylinderComponent Name=CollisionCylinder
		CollisionRadius=+084.000000
		CollisionHeight=+064.000000
		BlockNonZeroExtent=true
		BlockZeroExtent=true
		BlockActors=true
		CollideActors=true
		HiddenEditor=false
		HiddenGame=false
	End Object

	CollisionComponent=CollisionCylinder
	CylinderComponent=CollisionCylinder

	Components.Add(CollisionCylinder)


	Physics=PHYS_Projectile
	bApplyMagnetForce=false
	bCanBeMoved=true
	bLeftSide=true
	bRightSide=true
	bTopSide=true
	bBottomSide=true
	bFrontSide=true
	bBackSide=true
	bCollideWorld=true
	bCollideActors=true

	//Begin Object Class=DynamicLightEnvironmentComponent Name=MyLightEnvironment
	//		bEnabled=TRUE
	//	End Object
	//	LightEnvironment=MyLightEnvironment
	//	Components.Add(MyLightEnvironment)
	

	//Begin Object Class=StaticMeshComponent Name=StaticMeshComponent0
	//   // BlockRigidBody=true
	//	HiddenGame=true
	//	HiddenEditor=false
	//	LightEnvironment=MyLightEnvironment
	//	bUsePrecomputedShadows=FALSE
	//	End Object
	//CollisionComponent=StaticMeshComponent0
	//visualCue=StaticMeshComponent0
	//Components.Add(StaticMeshComponent0)
}
