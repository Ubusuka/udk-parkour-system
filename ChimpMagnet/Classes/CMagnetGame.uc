class CMagnetGame extends UDKGame;

DefaultProperties
{
	Acronym="CM"
	MapPrefixes[0]="CM"

	HUDType=class'CMHUD'
	PlayerControllerClass=class'CMPlayerController'
	ConsolePlayerControllerClass=class'CMPlayerController'
	DefaultPawnClass=class'CMPawn'


}
