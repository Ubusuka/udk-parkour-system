class CMHUD extends HUD;

//Pause Menu - Paul
var GFxMoviePlayer PauseMenuMovie;

var HUDMoviePlayer HudMovie;


event PostBeginPlay()
{
	super.PostBeginPlay();

	HudMovie = new class'HUDMoviePlayer';
	HudMovie.SetTimingMode(TM_Real);

	//HudMovie.InitHUDMovie(CMPlayerController(PlayerOwner));
}
event PostRender()
{
	super.PostRender();
	//HudMovie.TickHUD();
}
exec function TestFunc()
{

	//works

}

/** 
 *  Toggles visibility of normal in-game HUD
 */
function SetVisible(bool bNewVisible)
{
	bShowHUD = bNewVisible;
}

function ShowDebugInfo(out float out_YL, out float out_YPos)
{
    local CMPawn temp;
	local CMMagnetSide temp2;
    if( ShouldDisplayDebug( 'temp' ) )
    {
        foreach WorldInfo.AllPawns( class'CMPawn', temp )
        {
            temp.DisplayDebug( self, out_YL, out_YPos );
        }
    }
    else

    {

        Super.ShowDebugInfo( out_YL, out_YPos );

    }

	  //foreach WorldInfo.AllActors( class'CMMagnetSide', temp2 )
   //     {
   //         temp2.DisplayDebug( self, out_YL, out_YPos );
   //     }

}

exec function showMyMenu()
{
	PauseMenuTgl();
}

function PauseMenuTgl()
{
	if(PauseMenuMovie != none && PauseMenuMovie.bMovieIsOpen)
	{
		PlayerOwner.SetPause(False);
		PauseMenuMovie.Close(False); //keep the pause menu loaded in memory for reuse
		SetVisible(True);
	}
	else
	{
		PlayerOwner.SetPause(True);

		if(PauseMenuMovie == None)
		{
			PauseMenuMovie = new class'GFxMoviePlayer';

			PauseMenuMovie.MovieInfo = SWFMovie'ScaleformMenuGFx.SFMFrontEnd.SF_MainMenuv2';
			PauseMenuMovie.bEnableGammaCorrection = False;
			PauseMenuMovie.LocalPlayerOwnerIndex = class'Engine'.static.GetEngine().GamePlayers.Find(LocalPlayer(PlayerOwner.Player));
			PauseMenuMovie.SetTimingMode(TM_Real);
		}

		SetVisible(False);
		PauseMenuMovie.Start();
		PauseMenuMovie.Advance(0);

		if(!WorldInfo.IsPlayInMobilePreview())
		{
			PauseMenuMovie.AddFocusIgnoreKey('Escape');
		}
	}
}

DefaultProperties
{
	bShowDebugInfo=true
}
