class CMMagnetSide extends Actor placeable;

enum ECMBoxSide
{

	Box_Left,
	Box_Right ,
	Box_Front ,
	Box_Back,
	Box_Top ,
	Box_Bottom ,

	
};
enum ECMMagneticChargeC
{

	CMCharge_Negative,
	CMCharge_Neutral ,
	CMCharge_Positive ,
	
};

var(Magnet) StaticMeshComponent StaticMeshComponent;
var MaterialInstanceConstant MatInst;
var(Magnet) ECMMagneticChargeC theCharge;
var(Magnet) ECMBoxSide faceDirection;
var(Magnet) bool bMagnetActive;
var Actor currentAttractedActor;
var(Magnet) LinearColor chargeColours[3];


var bool bHasAttractor;
var vector magnetDirection;
var vector traceStart,traceEnd;


event PreBeginPlay()
{
	super.PreBeginPlay();

	//SetCollisionType(COLLIDE_TouchWeapons);
	
	CollisionComponent.SetAbsolute(false,false,false);
}


event PostBeginPlay()
{
	super.PostBeginPlay();

	if( StaticMeshComponent != none )
	{
		InitMaterialInstance();
	}

	InitChargeColours();

	magnetDirection=vector(Rotation);
	//switch(faceDirection)
	//{
	//case Box_Left:
	//	magnetDirection.X=1;
	//	break;
	//	case Box_Right:
	//		magnetDirection.X=-1;
	//	break;
	//	case Box_Front:
	//		magnetDirection.Y=1;
	//	break;
	//	case Box_Back:
	//		magnetDirection.Y=-1;
	//	break;
	//	case Box_Top:
	//		magnetDirection.Z=1;
	//	break;
	//	case Box_Bottom:
	//		magnetDirection.Z=-1;
	//	break;
	//}

		
	

	//CylinderComponent.SetTranslation(traceStart);

}

simulated function DisplayDebug(HUD Hud,out float out_YL, out float out_YPos)
{
	local String T;
	local Canvas canvas;

	canvas = Hud.Canvas;

	out_YPos += out_YL;

	 Canvas.SetPos(4, out_YPos);
    Canvas.SetDrawColor(255,0,0);
 
    T = "Player [" $ GetDebugName() $ "]";

	if(currentAttractedActor!=None)
	{
			T= " It is: " $currentAttractedActor.GetDebugName();
	}
	else
	{
		T="";
	}
	 Canvas.DrawText(GetDebugName() $ ": Magnet has attractor: "@bHasAttractor $ T, FALSE);
	 out_YPos += out_YL;

	 Canvas.SetPos(4, out_YPos);
    Canvas.SetDrawColor(255,0,0);
	Canvas.DrawText(GetDebugName() $ ":current charge"@theCharge, FALSE);
 

	
	//if(currentAttracedSide!=None)
 //   Canvas.DrawText(GetDebugName() $ ": Current traced magnet: "@currentAttracedSide.GetDebugName(), FALSE);
	//else
	//Canvas.DrawText(GetDebugName() $ ": Dont have a magnet traced", FALSE);

}
function InitChargeColours()
{
	chargeColours[0].A=0;
	chargeColours[0].R=1;
	chargeColours[0].G=0;
	chargeColours[0].B=0;

	chargeColours[1].A=0;
	chargeColours[1].R=0;
	chargeColours[1].G=0;
	chargeColours[1].B=1;

	chargeColours[2].A=0;
	chargeColours[2].R=0;
	chargeColours[2].G=1;
	chargeColours[2].B=0;
}
function InitMaterialInstance()
{
   MatInst = new(None) Class'MaterialInstanceConstant';
   MatInst.SetParent(StaticMeshComponent.GetMaterial(0));
   StaticMeshComponent.SetMaterial(0, MatInst);
   UpdateMaterialInstance();
}
function UpdateMaterialInstance()
{
	local LinearColor color;
	color.A=0;
	color.R=0;
	color.G=0;
	color.B=1;
   MatInst.SetVectorParameterValue('AmbientColor',color);
}
function Deactivate()
{

	//CMMagneticBox(Base).moveDirection.X=0;
	//CMMagneticBox(Base).moveDirection.Y=0;
	//CMMagneticBox(Base).moveDirection.Z=0;
}
function changeCharge(int armsCharge)
{
	
	local LinearColor color;

	if(theCharge==CMCharge_Neutral && ECMMagneticCharge(armsCharge)==CMCharge_Neutral)
	{
		return;
	}

	color.A=0;
	color.R=1;
	color.G=0;
	color.B=0;


	if(armsCharge==0 || armsCharge == 2)
	{
		if(Base != None)
	CMMagneticBox(Base).SetCurrentActiveSide(Self);
	//currentAttracedSide.Deactivate();

	}
	else
	{
		if(theCharge!=CMCharge_Neutral)
		{
			if(Base != None)
			{
			CMMagneticBox(Base).SetCurrentActiveSide(None);
			Deactivate();
			}
			//if(currentAttracedSide!=None)
			//currentAttracedSide.Deactivate();
		}
		////magnetDirection.X=0;
		////magnetDirection.Y=0;
		////magnetDirection.Z=0;
		
	}
		
	theCharge=ECMMagneticChargeC(armsCharge);

	 MatInst.SetVectorParameterValue('AmbientColor',chargeColours[int(theCharge)]);
	//Playing sound - Paul
	// SlideSound.Play();
}
function ToggleActivation()
{
	bMagnetActive=!bMagnetActive;
}
event Tick(float dTime)
{
	local vector hitLoc,hitNor;
	local Vector dir;
	local Vector zOffset;
	local Actor tracedActor;
	local vector newStart;
	local vector extent;
	local int traceIters;
	zOffset.Z=50;

	extent = (CollisionComponent.Bounds.BoxExtent)*0.5f;

	traceStart=Location;//+ vector(Rotation)*-30;
	traceEnd=traceStart + magnetDirection*32768;
	currentAttractedActor=None;


	//If the charge on this box is not neutral, we should carry on to see if anything will affect it.
	if(theCharge != CMCharge_Neutral)
	{

		foreach TraceActors(class'Actor', tracedActor, hitLoc, 
							hitNor, traceEnd, traceStart) 
		{
		
			if(tracedActor!=None)
			{
			
			
				if(tracedActor.IsA('CMMagnetSide'))
				{
					currentAttractedActor = tracedActor;
					//bHasAttractor=true;
					DrawDebugSphere(hitLoc,5,16,255,255,255);

					if(CMMagnetSide(tracedActor).theCharge == CMCharge_Neutral)
					{
						bHasAttractor=false;
						return;
					}
					
					currentAttractedActor=tracedActor;
					bHasAttractor=true;
						
					//	//if(Base != None && Base.IsA('CMMagneticBox'))
					//	//{
						
					if(Base != None)
					{
						if(CMMagnetSide(tracedActor).theCharge != theCharge)
						{
							CMMagneticBox(Base).moveDirection = magnetDirection;
						}
						else if(CMMagnetSide(tracedActor).theCharge == theCharge)
						{
							CMMagneticBox(Base).moveDirection = -magnetDirection;
						}
					}
							
							//CMMagneticBox(Base).bApplyMagnetForce = true;
							//CMMagneticBox(Base).forceDirection = magnetDirection;
							
						//}
					//break;
				}
				else if(tracedActor.IsA('CMPawn'))
				{
					continue;
				}
				else if(tracedActor.IsA('CMBanana'))
				{
					continue;
				}
				else
				{
				
					bHasAttractor=false;
				//	break;

				}


				if(traceIters ==1)
				{
					break;
				}
				traceIters++;

			}

		}
	}
	//If the charge on this box is not neutral, we should carry on to see if anything will affect it.
	//if(theCharge != CMCharge_Neutral)
	//{
		//DrawDebugLine(traceStart,traceEnd,255,255,0);
		
		
		//tracedActor=Trace(hitLoc,hitNor,traceEnd,traceStart,true);
		//if(tracedActor != None)
		//{
		//	currentAttractedActor=tracedActor;

		//	//DrawDebugBox(hitLoc,extent,255,255,255);
		//  DrawDebugSphere(hitLoc,5,16,255,255,255);
		//	//`log("Traced somthing "@tracedActor.Name);
		//	if(tracedActor.IsA('CMMagnetSide'))
		//	{
		//		bHasAttractor=true;
				
		//		//if(CMMagnetSide(tracedActor).theCharge == CMCharge_Neutral)
		//		//{
		//		//	bHasAttractor=false;
		//		//	return;
		//		//}

		//		//currentAttracedSide=CMMagnetSide(tracedActor);
		//		//	bHasAttractor=true;
					
		//		//	//if(Base != None && Base.IsA('CMMagneticBox'))
		//		//	//{
					
		//		//		if(CMMagnetSide(tracedActor).theCharge != theCharge)
		//		//		{
		//		//			CMMagneticBox(Base).moveDirection = magnetDirection;
		//		//		}
		//		//		else if(CMMagnetSide(tracedActor).theCharge == theCharge)
		//		//		{
		//		//			CMMagneticBox(Base).moveDirection = -magnetDirection;
		//		//		}
						
		//				//CMMagneticBox(Base).bApplyMagnetForce = true;
		//				//CMMagneticBox(Base).forceDirection = magnetDirection;
						
		//			//}
				
		//		//else
		//		//{
					
		//		//	CMMagneticBox(Base).moveDirection.X=0;
		//		//	CMMagneticBox(Base).moveDirection.Y=0;
		//		//	CMMagneticBox(Base).moveDirection.Z=0;
		//		//}
		//	}
		//	else
		//	{
		//		bHasAttractor=false;
		//		//currentAttracedSide=None;
		//		//CMMagneticBox(Base).moveDirection.X=0;
		//		//CMMagneticBox(Base).moveDirection.Y=0;
		//		//CMMagneticBox(Base).moveDirection.Z=0;
		//	}
		//}
		//else
		//{
		//	bHasAttractor=false;
		//	currentAttractedActor=None;
		//}
		
	//}
	//else
	//{
	//	CMMagneticBox(Base).moveDirection.X=0;
	//	CMMagneticBox(Base).moveDirection.Y=0;
	//	CMMagneticBox(Base).moveDirection.Z=0;
	//}

}

DefaultProperties
{


	theCharge= CMCharge_Neutral



		Begin Object Class=DynamicLightEnvironmentComponent Name=MyLightEnvironment
			bEnabled=TRUE
		End Object
		LightEnvironment=MyLightEnvironment
		Components.Add(MyLightEnvironment)


		Begin Object Class=StaticMeshComponent Name=StaticMeshComponent0
		LightEnvironment=MyLightEnvironment
		bUsePrecomputedShadows=FALSE
		BlockNonZeroExtent=true
		BlockZeroExtent=true
		//BlockActors=false
		//CollideActors=true
		End Object
	bCollideActors=true
	bBlockActors=false
	CollisionComponent=StaticMeshComponent0
	//bCollideWorld=true
	StaticMeshComponent=StaticMeshComponent0

	Components.Add(StaticMeshComponent0)
}
