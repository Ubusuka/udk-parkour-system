class CMMagneticBox extends Actor placeable;


//cpptext
//{
//#if WITH_EDITOR
//	// AActor interface.
//	virtual void EditorApplyScale(const FVector& DeltaScale, const FMatrix& ScaleMatrix, const FVector* PivotLocation, UBOOL bAltDown, UBOOL bShiftDown, UBOOL bCtrlDown);
//	virtual void CheckForErrors();
//#endif
//}
var	CylinderComponent		CylinderComponent;
var(Custom) StaticMeshComponent StaticMeshComponent;
var(Custom) DynamicLightEnvironmentComponent LightEnvironment;
var(Magnet) array<Actor> magnetSides;
//var(Magnet) array<Actor> customChildObjects;

//var MaterialInstanceConstant MatInst;
//var vector trc_left,trc_right,trc_up,trc_down,trc_forward,trc_backward;
//var LinearColor chargeColours[3];


var bool bJustBumpedMagnet;
//can this box be dragged towards another? Or is it rooted to the ground
var(Magnet) bool bCanBeMoved;
var Vector initialPosition;
var bool bHasActivatedSide;
var CMMagnetSide currentActiveSide;


var vector moveDirection;
//Paul sound variable
var AudioComponent SlideSound;
/*********************************************************************************************
***********       INIT FUNCTIONS          **************************************************
********************************************************************************************* */
event PreBeginPlay()
{
	
	local vector newPos;
	super.PreBeginPlay();
	initialPosition=Location;
	
}

event PostBeginPlay()
{
	
	local int a;

	super.PostBeginPlay();
	a=6;

	for(a=0;a<magnetSides.Length;a++)
	{
		if(magnetSides[a]!=None)
		{
			magnetSides[a].SetBase(self);
			//modularPieces[a].bHardAttach=true;
		}
	}

	SetCollisionType(COLLIDE_BlockAll);
	
	initialPosition.X=Location.X;
	initialPosition.Y=Location.Y;
	`log("Current Z "@Location.Z);
	initialPosition.Z=Location.Z;
	`log("Initial Z "@initialPosition.Z);
	
}
function resetMagPosition()
{
	
}


function SetCurrentActiveSide(CMMagnetSide newSide)
{
	if(newSide != None)
	{
		if(currentActiveSide!=None)
		{
			currentActiveSide.changeCharge(1);
		}
		currentActiveSide=newSide;
	}
	else
	{
		currentActiveSide=None;
	}

}
/*********************************************************************************************
***********       MISC FUNCTIONS          **************************************************
********************************************************************************************* */


/*********************************************************************************************
***********       TICKED FUNCTIONS          **************************************************
********************************************************************************************* */
event Tick(float dTime)
{

	//local vector moveDirection;
	local vector tempUp;
	local float distance;
	local int i;



	//initialise smallestDistance to a ridiculously high number so the first currentSmallestDistance check will be < less than it
	//smallestDistance = 100000000;
	if(currentActiveSide!=None)
	{
		if(currentActiveSide.bHasAttractor && bCanBeMoved)
		{
		 //`log("MOVE DIRECTION: "@moveDirection);
			Move((moveDirection*12.8f));
		}
	}

	//If the charge on this box is not neutral, we should carry on to see if anything will affect it.
	//if(theCharge!= CMCharge_Neutral)
	//{
	//	for(i=0;i<6;i++)
	//	{
	//		if(tracedActors[i] != None)
	//		{
	//			//Make sure what we traced is a magnet
	//			if(tracedActors[i].isA('CMMagneticBox'))
	//			{
	//				//If the traced box is neutral, we dont want to know about it, just continue to the next iteration of the loop
	//				if(CMMagneticBox(tracedActors[i]).theCharge == CMCharge_Neutral)
	//				{
	//					continue;
	//				}
	//				else
	//				{
	//					//if(bumpedMagnet!=None)
	//					//{
	//					//	//if the current traced magnet is the same magnet as we already bumped into and they still have oppposite charges, we dont want to do anything in that direction
	//					//	if(bumpedMagnet == CMMagneticBox(tracedActors[i]) && bumpedMagnet.theCharge != CMMagneticBox(tracedActors[i]).theCharge)
	//					//	{
	//					//		//bumpedMagnet=None;
	//					//		continue;
	//					//	}
	//					//	else
	//					//	{
	//					//		bumpedMagnet=None;
	//					//	}
	//					//}


	//					//currentSmallestDistance is the smallest distance between any magnets in the 6 directions
	//					//We keep looping through each of the directions only storing a new magnet affector if the distance is less than the previous minimum
	//					currentSmallestDistance = VSize(CMMagneticBox(tracedActors[i]).Location - Location);
	//					if(currentSmallestDistance <= smallestDistance)
	//					{
	//						smallestDistance = currentSmallestDistance;
	//						//if(tracedActors[i].
	//						affector=CMMagneticBox(tracedActors[i]);
							
	//					}
	//				}
	//			}
	//		}
	//	}
	//}
	//else
	//{
	//	affector=None;
	//	return;
	//}
	
	////If this magnet has a chance to be affected by another
	//if(affector!=None)
	//{

	
	//	//store current distance to that affector, used for magnet force
	//	distance=VSize(affector.Location-Location);
	//	tempDistance=distance;
	//	distance = 1.0f/distance;
	//	distance*=600;
	//	//Make sure neither of the magnets charge has changed to neutral during the frame
	//	if(theCharge != CMCharge_Neutral && affector.theCharge != CMCharge_Neutral)
	//	{
			
	//		if(bCanBeMoved)//a boolean that can be set which will be used to determine a static magnet
	//		{
	//			//MOVE DIRECTION IS INFO GIVEN FRM THE ACTUAL MAGNET SEMISPHERE TO ITS PARENT(THIS)



	//			/*THIS WORKS FINE IF THE ACTOR IS NOT INTERSECTING WITH ANYTHING ELSE AT ALL, MAKE SURE
	//				* THE ENTIRE MESH IS UNOBSTRUCTED*/
	//				if(int(affector.theCharge) != int(theCharge))
	//				{
	//					//Velocity = (moveDirection*128)*(dTime*distance*600);
	//					//Latent move function to move the magnet based on distance
	//					//MoveSmooth((moveDirection*600)*(dTime*distance));
	//					Move((moveDirection*12.8f));
				
	//				}
	//				else
	//				{
	//					//Velocity = (-1*moveDirection*128)*(dTime*distance*600);
	//					//MoveSmooth((-1*moveDirection*600)*(dTime*distance));
	//					Move((-moveDirection*12.8f));
	//				}
			
				
	//		}
			
	//	}

		
	//}
	
	
	//DrawDebugLines();

}

/*********************************************************************************************
***********      DEBUG FUNCTIONS          **************************************************
********************************************************************************************* */
simulated function DisplayDebug(HUD Hud,out float out_YL, out float out_YPos)
{
	//local String T;
	//local Canvas canvas;

	//canvas = Hud.Canvas;

	//out_YPos += out_YL;

	// Canvas.SetPos(400, out_YPos);
 //   Canvas.SetDrawColor(255,0,0);
 
	//if(affector!=None)
	//{
	//	T = "This magnet traced" $ affector.GetDebugName() $ "]";
	//}
	//else
	//{
	//	T = "";
	//}

 //   Canvas.DrawText(T, FALSE);

}
function DrawDebugLines()
{
	//local vector centre,tempUp;
	////tempUp.Z = 100;
	//centre = StaticMeshComponent.GetPosition();
	//centre+=tempUp;

	//DrawDebugLine(centre,centre + trc_up*400,1,0,0);
	//DrawDebugLine(centre,centre + (trc_down*400),1,0,0);

	//DrawDebugLine(centre,centre + trc_left*400,1,0,0);
	//DrawDebugLine(centre,centre + (trc_right*400),1,0,0);


	//DrawDebugLine(centre,centre + trc_forward*400,1,0,0);
	//DrawDebugLine(centre,centre + (trc_backward*400),1,0,0);

}


DefaultProperties
{
	Begin Object class=AudioComponent name=ChargeAudioComponent
		SoundCue = SoundCue'Chimp_Magnet.Sound.Sliding_Cue'
	End Object
	SlideSound = ChargeAudioComponent


	bHasActivatedSide=false
	Physics=PHYS_Projectile
	bJustBumpedMagnet=false
	bCanBeMoved = true
	bCollideWorld=true
	bCollideActors=true
	bBlockActors=true
	//smallestDistance=100000;
	

		Begin Object Class=DynamicLightEnvironmentComponent Name=MyLightEnvironment
			bEnabled=TRUE
		End Object
		LightEnvironment=MyLightEnvironment
		Components.Add(MyLightEnvironment)


		Begin Object Class=StaticMeshComponent Name=StaticMeshComponent0
	   // BlockRigidBody=true
		LightEnvironment=MyLightEnvironment
		bUsePrecomputedShadows=FALSE
		End Object
	//CollisionComponent=StaticMeshComponent0
	StaticMeshComponent=StaticMeshComponent0

	Begin Object Class=CylinderComponent Name=CollisionCylinder
		CollisionRadius=+0100.000000
		CollisionHeight=+0100.000000
		BlockNonZeroExtent=true
		BlockZeroExtent=true
		BlockActors=true
		CollideActors=true
		HiddenEditor=false
		HiddenGame=false
	End Object

	CollisionComponent=CollisionCylinder
	CylinderComponent=CollisionCylinder

	Components.Add(CollisionCylinder)
	//CollisionComponent=StaticMeshComponent0
	Components.Add(StaticMeshComponent0)

}
