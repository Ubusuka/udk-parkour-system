class CMMagnetModularSide extends InterpActor;

enum ECMBoxSide
{

	Box_Left,
	Box_Right ,
	Box_Front ,
	Box_Back,
	Box_Top ,
	Box_Bottom ,

	
};

var(Magnet) bool isAMagnetSide;
var(Magnet) ECMBoxSide faceDirection;
var(Magnet) bool bMagnetActive;
var vector magnetDirection;
var(Trace) float tracestartAmount,traceZOffset;
var vector traceStart,traceEnd;

var	CylinderComponent		CylinderComponent;
event PreBeginPlay()
{
	super.PreBeginPlay();

	SetCollisionType(ECollisionType.COLLIDE_BlockAll);
	
	CollisionComponent.SetAbsolute(false,false,false);
}
event PostBeginPlay()
{
	super.PostBeginPlay();
	switch(faceDirection)
	{
	case Box_Left:
		magnetDirection.X=1;
		break;
		case Box_Right:
			magnetDirection.X=-1;
		break;
		case Box_Front:
			magnetDirection.Y=1;
		break;
		case Box_Back:
			magnetDirection.Y=-1;
		break;
		case Box_Top:
			magnetDirection.Z=1;
		break;
		case Box_Bottom:
			magnetDirection.Z=-1;
		break;
	}

		
	

	//CylinderComponent.SetTranslation(traceStart);

}
function Activate()
{
}
function Deactivate()
{
}

//event Bump(Actor other,PrimitiveComponent otherComp,vector hitNorm)
//{
	
//if(Base != None)
//	{
					
//		//`log("Ye boi");
//		if(Base.IsA('CMMagnet'))
//		{
//			`log("Bump");
//			CMMagnet(Base).bApplyMagnetForce = false;
//			CMMagnet(Base).forceDirection.X=0;
//			CMMagnet(Base).forceDirection.Y=0;
//			CMMagnet(Base).forceDirection.Z=0;
//		}
//	}
//}
//event HitWall(Vector hitnormal,Actor wall,PrimitiveComponent wallComp)
//{
//	if(Base != None)
//	{
					
//		//`log("Ye boi");
//		if(Base.IsA('CMMagnet'))
//		{
//			`log("HIT");
//			CMMagnet(Base).bApplyMagnetForce = false;
//			CMMagnet(Base).forceDirection.X=0;
//			CMMagnet(Base).forceDirection.Y=0;
//			CMMagnet(Base).forceDirection.Z=0;
//		}
//	}
	
	
//}
event Tick(float dTime)
{
	local vector hitLoc,hitNor;
	local Vector dir;
	local Vector zOffset;
	local Actor tracedActor;
	local vector newStart;
	zOffset.Z=50;

	traceStart=Location+magnetDirection*tracestartAmount;
		traceStart.Z=Location.Z+traceZOffset;
	traceEnd=traceStart + magnetDirection*1000;

	if(isAMagnetSide && bMagnetActive)
	{
		DrawDebugLine(traceStart,traceEnd,1,0,0);
		
		tracedActor=Trace(hitLoc,hitNor,traceEnd,traceStart);
		if(tracedActor != None)
		{
			//`log("Traced somthing "@tracedActor.Name);
			if(tracedActor.IsA('CMMagnetModularSide'))
			{
				/*TELL JAY HIS STUFF IS SPASTIC AND TO JUST DECIDE WHAT BOXES HE WANTS AND CREATE THEM IN MAX, THEN ITS FUCKING SIMPLE*/
				if(CMMagnetModularSide(tracedActor).bMagnetActive && CMMagnetModularSide(tracedActor).isAMagnetSide)
				{
					
					if(Base != None)
					{
					
						`log("Ye boi");
						if(Base.IsA('CMMagnet'))
						{
						CMMagnet(Base).bApplyMagnetForce = true;
						CMMagnet(Base).forceDirection = magnetDirection;
						}
					}
				}
			}
			else if(tracedActor.IsA('CMMagnet'))
			{
				newStart=hitLoc;
				traceEnd=newStart+ magnetDirection*1000;
				tracedActor=Trace(hitLoc,hitNor,traceEnd,newStart);
				if(tracedActor != None)
				{
					if(tracedActor.IsA('CMMagnetModularSide'))
					{
						if(CMMagnetModularSide(tracedActor).bMagnetActive && CMMagnetModularSide(tracedActor).isAMagnetSide)
						{
					
							`log("2nd way");
							if(Base != None)
							{
					
								`log("Ye boi");
								if(Base.IsA('CMMagnet'))
								{
								CMMagnet(Base).bApplyMagnetForce = true;
								CMMagnet(Base).forceDirection = magnetDirection;
								}
							}
						}
					}
				}
			}
			//else if(tracedActor.IsA(''))
			//{

			//}
			
		}
		
	}

}
simulated function DisplayDebug(HUD Hud,out float out_YL, out float out_YPos)
{
	local String T;
	local Canvas canvas;
	local int a;

	canvas = Hud.Canvas;

	 //for(a=0;a<defaultPrefab.PrefabArchetypes.Length;a++)
	 //{

		out_YPos += out_YL;
		Canvas.SetPos(4, out_YPos);
    Canvas.SetDrawColor(255,0,0);
	 Canvas.DrawText("", FALSE);

	 //}
   

}
function changeDirection()
{
	if(Base.IsA('CMMagnet'))
	{
		CMMagnet(Base).tmpMoveDir*= -1;
	}
}
DefaultProperties
{
	//Begin Object Class=CylinderComponent Name=CollisionCylinder
	//	CollisionRadius=+0078.000000
	//	CollisionHeight=+0078.000000
	//	BlockNonZeroExtent=true
	//	BlockZeroExtent=true
	//	BlockActors=true
	//	CollideActors=true
	//	HiddenEditor=false
	//	HiddenGame=false
	//End Object

	//CollisionComponent=CollisionCylinder
	//CylinderComponent=CollisionCylinder

	//Components.Add(CollisionCylinder)
	//bCollideWorld=true
	bBlockActors=true
	bCollideActors=true
	traceZOffset=100
	tracestartAmount=200
	bMagnetActive=false
	isAMagnetSide=false

	//bCollideAsEncroacher=false
}
