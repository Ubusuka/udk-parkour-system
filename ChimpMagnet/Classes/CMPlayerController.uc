class CMPlayerController extends PlayerController ;


var bool inTestState,inNormalState;

var bool bStoppedSprint;

var bool bJumpKeyIsUp;
var bool bChargingJump;
var vector bestEdgeLocation;//used for storing the edge needed to vault to


var bool bJustJumped;
var vector lastKnownAcceleration,lastKnownVelocity;
var float lastKnownAccelRate;
var int leftTurn;
//deceleration variables
var bool slowdown;
var int slowdowntimercount;
var float magnitude;
var Vector direction;

//Pauls Sounds
var AudioComponent SprintingSound, JumpSound, ShootSound, ChangeChargeSound;


state LadderClimb
{
	local vector newPos;

	local EPhysics previousPhysicsState;

	function UpdateRotation( float DeltaTime )
	{
			local Rotator	DeltaRot, newRotation, ViewRotation;

			ViewRotation = Rotation;
			if (Pawn!=none)
			{
				Pawn.SetDesiredRotation(ViewRotation);
			}

				DeltaRot.Yaw	= PlayerInput.aTurn * 0.2f;
				DeltaRot.Pitch	= PlayerInput.aLookUp* 0.4f;
			ProcessViewRotation( DeltaTime, ViewRotation, DeltaRot );
			SetRotation(ViewRotation);

			ViewShake( deltaTime );

			NewRotation = ViewRotation;
			NewRotation.Roll = Rotation.Roll;

			if ( Pawn != None )
				Pawn.FaceRotation(NewRotation, deltatime);
	}
	function ProcessMove(float DeltaTime, vector NewAccel, eDoubleClickDir DoubleClickMove, rotator DeltaRot)
	{
		
		if( Pawn == None )
		{
			return;
		}

		if (Role == ROLE_Authority)
		{
			// Update ViewPitch for remote clients
			Pawn.SetRemoteViewPitch( Rotation.Pitch );
		}

		Pawn.Acceleration = NewAccel;

	}
	function PlayerMove( float DeltaTime )
	{
		local vector			X,Y,Z, NewAccel;
		local eDoubleClickDir	DoubleClickMove;
		local rotator			OldRotation;
		local bool				bSaveJump;

		if( Pawn == None )
		{
			GotoState('Dead');
		}
		else
		{
			GetAxes(Pawn.Rotation,X,Y,Z);

			
			// Update acceleration.
			NewAccel = PlayerInput.aForward*Z;//+ PlayerInput.aStrafe*Y;
			NewAccel = Pawn.AccelRate * Normal(NewAccel);

			if (IsLocalPlayerController())
			{
				AdjustPlayerWalkingMoveAccel(NewAccel);
			}

			DoubleClickMove = PlayerInput.CheckForDoubleClickMove( DeltaTime/WorldInfo.TimeDilation );

			// Update rotation.
			OldRotation = Rotation;
			UpdateRotation( DeltaTime );

			if( Role < ROLE_Authority ) // then save this move and replicate it
			{
				ReplicateMove(DeltaTime, NewAccel, DoubleClickMove, OldRotation - Rotation);
			}
			else
			{
				ProcessMove(DeltaTime, NewAccel, DoubleClickMove, OldRotation - Rotation);
			}
		}
	}

	event PlayerTick(float dTime)
	{
	
		
		Pawn.Mesh.PlayAnim('Ladder',0.3f,false,false);
		
		Pawn.Velocity.X=0;
		Pawn.Velocity.Y=0;

		super.PlayerTick(dTime);

	}
	event BeginState(name PreviousStateName)
	{
		local vector newPos;
		previousPhysicsState=Pawn.Physics;
		`log("In ladder");
		
	
		Pawn.Velocity.X=0;
		Pawn.Velocity.Y=0;
		Pawn.Velocity.Z=0;
		Pawn.Acceleration.X=0;
		Pawn.Acceleration.Y=0;
		Pawn.Acceleration.Z=0;
	}
	event EndState(name nextStateName)
	{
		`log("Out ladder");
		Pawn.Mesh.StopAnim();
		Pawn.SetPhysics(previousPhysicsState);
	}
Begin:
	Pawn.SetLocation(Pawn.Location+ vector(CMPawn(Pawn).magnetInteractiveSide.Rotation));

}
state MonkeySwing
{

	local EPhysics previousPhysicsState;

	function UpdateRotation( float DeltaTime )
	{
			local Rotator	DeltaRot, newRotation, ViewRotation;

			ViewRotation = Rotation;
			if (Pawn!=none)
			{
				Pawn.SetDesiredRotation(ViewRotation);
			}

				DeltaRot.Yaw	= PlayerInput.aTurn * 0.2f;
				DeltaRot.Pitch	= PlayerInput.aLookUp* 0.4f;
			ProcessViewRotation( DeltaTime, ViewRotation, DeltaRot );
			SetRotation(ViewRotation);

			ViewShake( deltaTime );

			NewRotation = ViewRotation;
			NewRotation.Roll = Rotation.Roll;

			if ( Pawn != None )
				Pawn.FaceRotation(NewRotation, deltatime);
	}
	function ProcessMove(float DeltaTime, vector NewAccel, eDoubleClickDir DoubleClickMove, rotator DeltaRot)
	{
		
		if( Pawn == None )
		{
			return;
		}

		if (Role == ROLE_Authority)
		{
			// Update ViewPitch for remote clients
			Pawn.SetRemoteViewPitch( Rotation.Pitch );
		}

		Pawn.Acceleration = NewAccel;

	}
	function PlayerMove( float DeltaTime )
	{
		local vector			X,Y,Z, NewAccel;
		local eDoubleClickDir	DoubleClickMove;
		local rotator			OldRotation;
		local bool				bSaveJump;

		if( Pawn == None )
		{
			GotoState('Dead');
		}
		else
		{
			GetAxes(Pawn.Rotation,X,Y,Z);

			//`log("here");
			// Update acceleration.
			NewAccel = PlayerInput.aForward*X ;//+ PlayerInput.aStrafe*Y;
			NewAccel.Z	= 0;
			NewAccel = Pawn.AccelRate * Normal(NewAccel);

			if (IsLocalPlayerController())
			{
				AdjustPlayerWalkingMoveAccel(NewAccel);
			}

			DoubleClickMove = PlayerInput.CheckForDoubleClickMove( DeltaTime/WorldInfo.TimeDilation );

			// Update rotation.
			OldRotation = Rotation;
			UpdateRotation( DeltaTime );

			if( Role < ROLE_Authority ) // then save this move and replicate it
			{
				ReplicateMove(DeltaTime, NewAccel, DoubleClickMove, OldRotation - Rotation);
			}
			else
			{
				ProcessMove(DeltaTime, NewAccel, DoubleClickMove, OldRotation - Rotation);
			}
		}
	}

	event PlayerTick(float dTime)
	{
	
		local vector newPos;
		Pawn.Mesh.PlayAnim('MonkeyBars',0.3f,false,false);
		newPos=Pawn.Location;
		newPos.Z=CMPawn(Pawn).magnetInteractiveSide.Location.Z;
		
		Pawn.SetLocation(newPos +vector(CMPawn(Pawn).magnetInteractiveSide.Rotation) * CMPawn(Pawn).CylinderComponent.CollisionHeight );
		Pawn.Velocity.Z=0;
		Pawn.Acceleration.Z=0;

		super.PlayerTick(dTime);

	}
	event BeginState(name PreviousStateName)
	{
		previousPhysicsState=Pawn.Physics;
		
		//`log("In Monkey");
		//Pawn.SetPhysics(PHYS_Flying);
		Pawn.Velocity.X=0;
		Pawn.Velocity.Y=0;
		Pawn.Velocity.Z=0;
		Pawn.Acceleration.X=0;
		Pawn.Acceleration.Y=0;
		Pawn.Acceleration.Z=0;
	}
	event EndState(name nextStateName)
	{
		Pawn.Mesh.StopAnim();
		//`log("Out Monkey");
		//Pawn.SetCollision(true,true,true);
		Pawn.Velocity.Z=-200;
		Pawn.SetPhysics(previousPhysicsState);
	}
}
state PowerSlide
{
	local name previousState;
	local float timeSliding;
	local vector startVelocity;

	function UpdateRotation( float DeltaTime )
	{
			local Rotator	DeltaRot, newRotation, ViewRotation;

			ViewRotation = Rotation;
			if (Pawn!=none)
			{
				Pawn.SetDesiredRotation(ViewRotation);
			}

			//	DeltaRot.Yaw	= PlayerInput.aTurn * 0.1f;
				//DeltaRot.Pitch	= PlayerInput.aLookUp* 0.1f;
			ProcessViewRotation( DeltaTime, ViewRotation, DeltaRot );
			SetRotation(ViewRotation);

			ViewShake( deltaTime );

			NewRotation = ViewRotation;
			NewRotation.Roll = Rotation.Roll;

			if ( Pawn != None )
				Pawn.FaceRotation(NewRotation, deltatime);
	}
	event PlayerTick(float dTime)
	{
		//`log("yep");
		local vector hitLocation,HitNorm,tracestart,traceend,offsettrace;
		local vector			X,Y,Z;
		

		GetAxes(CMPawn(Pawn).cameraRotation,X,Y,Z);

		
		offsettrace.Z=(CMPawn(Pawn).CylinderComponent.CollisionHeight/2)*5;
		tracestart=CMPawn(Pawn).Location;
		tracestart.X=CMPawn(Pawn).Location.X + (CMPawn(Pawn).CylinderComponent.CollisionRadius*(-X.X));
		tracestart.Z=CMPawn(Pawn).Location.Z + CMPawn(Pawn).BaseEyeHeight;
		traceend=tracestart+offsettrace;

		DrawDebugLine(tracestart,traceend,0,0,255);
		super.PlayerTick(dTime);
		timeSliding+=dTime;

		Pawn.Velocity = startVelocity+vector(Pawn.Rotation) * 200;
		Pawn.Mesh.PlayAnim('Slide',0.6f,false,true);
		
		if(Trace(hitLocation,HitNorm,tracestart,traceend,true) == None)
		{
			//`log("NOPE");
			if(timeSliding > 0.5f)
			{
				GoToState(previousState);
			}
		}
		else
		{
			//`log("YES");
		}
	}

	event BeginState(name PreviousStateName)
	{
		timeSliding=0;
		`log("In PowerSlide");
		startVelocity=Pawn.Velocity;
		CMPawn(Pawn).ShrinkCollision();
		//CMPawn(Pawn)
		previousState=PreviousStateName;
		//goto('Ending');
	}
	event EndState(name nextStateName)
	{
		`log("Out PowerSlide");
		CMPawn(Pawn).GrowCollision();
	}

	

}
state WallJump
{
	local name previousState;
	local vector newDirection;
	//override this function to disallow player input from moving the camera during the quick turn
	function UpdateRotation( float DeltaTime )
	{
			local Rotator	DeltaRot, newRotation, ViewRotation;

			ViewRotation = Rotation;
			if (Pawn!=none)
			{
				Pawn.SetDesiredRotation(ViewRotation);
			}

			ProcessViewRotation( DeltaTime, ViewRotation, DeltaRot );
			SetRotation(ViewRotation);

			ViewShake( deltaTime );

			NewRotation = ViewRotation;
			NewRotation.Roll = Rotation.Roll;

			if ( Pawn != None )
				Pawn.FaceRotation(NewRotation, deltatime);
	}
	event PlayerTick(float dTime)
	{
		
		super.PlayerTick(dTime);

		Pawn.SetViewRotation(RInterpTo(Rotation,Rotator(newDirection),dTime,100000,true));

		if(RSize(Rotator(newDirection) - Rotation) == 0)
		{
			
			GoToState(previousState);
		}
		
	}
	event BeginState(name PreviousStateName)
	{
		newDirection=MirrorVectorByNormal(vector(Rotation),CMPawn(Pawn).wallJumpHitNormal);
		`log("In Walljump");
		CMPawn(Pawn).DoWallJump(true);
		previousState=PreviousStateName;
	}
	event EndState(name nextStateName)
	{
		`log("Out Walljump");
	}

}
state QuickTurn
{

	local name previousState;
	local vector directionToTurn;
	local vector accelToApply;
	local vector startAccel;
	local vector startVelocity;
	local float startAccelMag,startVelMag;
	event PlayerTick(float dTime)
	{
		local Rotator faceRotation;
		local vector X,Y,Z;

		super.PlayerTick(dTime);

		if(leftTurn == 0)
		{
			//lerp from the current rotation to the final destination
			Pawn.SetViewRotation(RInterpTo(Rotation,Rotator(directionToTurn),dTime,100000,true));
			//Pawn.Acceleration += accelToApply*250;
		}
		else if(leftTurn == 1)
		{
			Pawn.SetViewRotation(RInterpTo(Rotation,Rotator(-directionToTurn),dTime,100000,true));
			//Pawn.Acceleration += (-accelToApply)*250;
		}

	

		//if the distance between the rotation we wanted to turn too and the current rotation is 0, we have reached the destination
		if(RSize(Rotator(directionToTurn) - Rotation) == 0 || RSize(Rotator(-directionToTurn) - Rotation) == 0)
		{
			GoToState(previousState);
		}
		
	}

	//override this function to disallow player input from moving the camera during the quick turn
	function UpdateRotation( float DeltaTime )
	{
			local Rotator	DeltaRot, newRotation, ViewRotation;

			ViewRotation = Rotation;
			if (Pawn!=none)
			{
				Pawn.SetDesiredRotation(ViewRotation);
			}

			ProcessViewRotation( DeltaTime, ViewRotation, DeltaRot );
			SetRotation(ViewRotation);

			ViewShake( deltaTime );

			NewRotation = ViewRotation;
			NewRotation.Roll = Rotation.Roll;

			if ( Pawn != None )
				Pawn.FaceRotation(NewRotation, deltatime);
	}
	event BeginState(name PreviousStateName)
	{
		local vector X,Y,Z;
		local Rotator currentCamRot;
		`log("in state" @GetStateName());
		if(Pawn != None)
		{
			//store initial velocity and acceleration so we can apply it back to the player when the state ends with the desired reduction due to the vault
		startAccel=Pawn.Acceleration;
		startVelocity=Pawn.Velocity;
		startAccelMag=VSize(startAccel);
		startVelMag=VSize(startVelocity);
		}


		GetPlayerViewPoint(X,currentCamRot);
		GetAxes(currentCamRot,X,directionToTurn,Z);

		if(leftTurn == 0)
		{
		accelToApply = Normal(X + directionToTurn);
		}
		else if(leftTurn==1)
		{
			accelToApply = Normal(X + (-directionToTurn));
		}

		previousState=PreviousStateName;
	}
	event EndState(name nextStateName)
	{
		if(Pawn.Physics != PHYS_Falling)
		{
		
		//Pawn.Acceleration=(startAccel*200)+( accelToApply * startAccelMag);
			//Pawn.Acceleration*=0.25f;
		Pawn.Velocity=(startVelocity*19500) + (accelToApply*startVelMag);
		}
		//reset leftTurn variable when leaving quickturn incase the same value is carried over from the previous
		leftTurn = -1;
	}
}
state Vault
{
	local vector startAccel;
	local vector startVelocity;
	function PlayerMove( float DeltaTime )
	{
		//this is overridden to stop the player from using input to move during any vaulting state.
	}
	event PlayerTick(float dTime)
	{
		super.PlayerTick(dTime);
		

	}
	event BeginState(name PreviousStateName)
	{
		if(Pawn != None)
		{
			//store initial velocity and acceleration so we can apply it back to the player when the state ends with the desired reduction due to the vault
		startAccel=Pawn.Acceleration;
		startVelocity=Pawn.Velocity;
		Pawn.Velocity.X=0;
		Pawn.Velocity.Y=0;
		Pawn.Velocity.Z=0;
		Pawn.Acceleration.X=0;
		Pawn.Acceleration.Y=0;
		Pawn.Acceleration.Z=0;
		Pawn.SetPhysics(PHYS_Flying);
		}
		
		
		`log("in state "@GetStateName());
		
	}
	event endState(name NextStateName)
	{
		
		if(Pawn != None)
		{
		Pawn.Acceleration=startAccel;
		Pawn.Velocity=startVelocity;
		Pawn.SetPhysics(PHYS_Walking);
		
		}
		`log("out state"@GetStateName());
	}

}
state HighVault extends Vault
{
	event PlayerTick(float dTime)
	{

		local float desiredZ;
		local vector playerPositionNoZ/*Position of the player with 0 Z value, used to get direction to move in(only need x and y)*/;

		local vector up;
		local vector baseObstacle,playerForwardVec;

		

		super.PlayerTick(dTime);

					up.Z = 1;
		baseObstacle=CMPawn(Pawn).bestEdgeLocation;
		//baseObstacle.Z=0;
		
		playerPositionNoZ = Pawn.Location;
		playerPositionNoZ.Z=baseObstacle.Z;
		playerForwardVec = Normal(baseObstacle - playerPositionNoZ);
		if(CMPawn(Pawn).bFoundEdge)
		{

		desiredZ = (CMPawn(Pawn).bestEdgeLocation).Z;

		
		
		Pawn.Velocity = (Normal(up+playerForwardVec)*500);
		

		//if(CMPawn(Pawn).InMiddleRange())
		//{
			Pawn.Mesh.PlayAnim('Vault',1.0f,false,false);

		//}
			//if((CMPawn(Pawn).kneeHeightOffset.Z > desiredZ) || !CMPawn(Pawn).bFoundEdge)
			//{
				
			//	if(!bStoppedSprint)
			//	{
			//	GoToState('PlayerSprinting');
			//	}
			//	else
			//		GoToState('PlayerWalking');

			//}
		}
		else
		{
			
			if(!bStoppedSprint)
			{
				GoToState('PlayerSprinting');
			}
			else
				GoToState('PlayerWalking');
		}

		//`log("current physics" @Pawn.Physics);
	}

	event endState(name NextStateName)
	{
		
		if(Pawn != None)
		{
		Pawn.Acceleration=startAccel*0.2f;
		Pawn.Velocity=startVelocity*0.2f;
		Pawn.SetPhysics(PHYS_Walking);
		
		}
		`log("out state"@GetStateName());
	}
}
state HandVault extends Vault
{
	local vector directionToLook;
	local Rotator initialLook;
	event PlayerTick(float dTime)
	{
				local float desiredZ;
		local vector playerPositionNoZ/*Position of the player with 0 Z value, used to get direction to move in(only need x and y)*/;

		local vector up;
		local vector baseObstacle,playerForwardVec;

		

		super.PlayerTick(dTime);

					up.Z = 1;
		baseObstacle=CMPawn(Pawn).bestEdgeLocation;
		playerPositionNoZ = Pawn.Location;
		playerPositionNoZ.Z=baseObstacle.Z;
		playerForwardVec = Normal(baseObstacle - playerPositionNoZ);
		if(CMPawn(Pawn).bFoundEdge)
		{

		desiredZ = (CMPawn(Pawn).bestEdgeLocation).Z;

		//TODO: Player can "vault" by pressing sprint next to the obstacle and also release the sprint button, so they are sent back to the sprint state without sprint being pressed
		//Pawn.MoveSmooth((desiredPosition+rot)*dTime*5.0f);
		Pawn.Velocity = (Normal(up+playerForwardVec)*800);
		Pawn.Mesh.PlayAnim('Vault',0.4f,false,false);
		//Pawn.SetViewRotation(RInterpTo(Rotation,Rotator(directionToLook),dTime,100000,true));
		}
		else
		{
			
			if(!bStoppedSprint)
			{
				GoToState('PlayerSprinting');
			}
			else
				GoToState('PlayerWalking');
		}

		//`log("current physics" @Pawn.Physics);
	}
	event BeginState(name PreviousStateName)
	{
		local vector temp;
		super.BeginState(PreviousStateName);
		//GetPlayerViewPoint(temp,initialLook);
		//directionToLook = (initialLook + (Normal(CMPawn(Pawn).bestEdgeLocation-temp)));
	}
	event endState(name NextStateName)
	{
		
		if(Pawn != None)
		{
		Pawn.Acceleration=startAccel*0.75f;
		Pawn.Velocity=startVelocity*0.75f;
		Pawn.SetPhysics(PHYS_Walking);
		
		}
		`log("out state"@GetStateName());
	}
}
state QuickVault extends Vault
{

	
	function PlayerMove( float DeltaTime )
	{
		local vector			X,Y,Z, NewAccel;
		local eDoubleClickDir	DoubleClickMove;
		local rotator			OldRotation;

		if( Pawn == None )
		{
			GotoState('Dead');
		}
		else
		{
			GetAxes(Pawn.Rotation,X,Y,Z);

			//// Update rotation.
			OldRotation = Rotation;
			UpdateRotation( DeltaTime );

		}

	}

	event PlayerTick(float dTime)
	{
		local float desiredZ;
		local vector playerPositionNoZ/*Position of the player with 0 Z value, used to get direction to move in(only need x and y)*/;

		local vector up;
		local vector baseObstacle,obstacleUpVec,playerForwardVec;


		//local vector offset;
		//local vector rot;
		super.PlayerTick(dTime);

		up.Z = 1;
		baseObstacle=CMPawn(Pawn).bestEdgeLocation;
		//baseObstacle.Z=0;
		obstacleUpVec=Normal(CMPawn(Pawn).bestEdgeLocation-baseObstacle);
		playerPositionNoZ = Pawn.Location;
		playerPositionNoZ.Z=baseObstacle.Z;
		playerForwardVec = Normal(baseObstacle - playerPositionNoZ);

		//DrawDebugLine(playerPositionNoZ,baseObstacle+up+playerForwardVec,0,255,0,true);
		//DrawDebugCoordinateSystem(
		//GoToState('PlayerWalking');
		if(CMPawn(Pawn).bFoundEdge)
		{
		//rot = vector(Pawn.Rotation) * Pawn.GetCollisionRadius();
		desiredZ = (CMPawn(Pawn).bestEdgeLocation).Z;

		
		//offset =CMPawn(Pawn).bestEdgeLocation;
		//offset = offset + rot;
		
		//TODO: Player can "vault" by pressing sprint next to the obstacle and also release the sprint button, so they are sent back to the sprint state without sprint being pressed
		Pawn.Velocity = (Normal(up+playerForwardVec)*800); //Maybe I need delta time here? Not sure if it is handled somewhere else

		}
		else
		{
			
			if(!bStoppedSprint)
			{
				GoToState('PlayerSprinting');
			}
			else
				GoToState('PlayerWalking');
		}

		//`log("current physics" @Pawn.Physics);
	}
	event EndState(name NextStateName)
	{
		super.EndState(nextStateName);

		//if(Pawn != None)
		//CMPawn(Pawn).DoJump(true);
	}

}
state PlayerWalking
{
ignores SeePlayer, HearNoise, Bump;

	event NotifyPhysicsVolumeChange( PhysicsVolume NewVolume )
	{
		if ( NewVolume.bWaterVolume && Pawn.bCollideWorld )
		{
			GotoState(Pawn.WaterMovementState);
		}
	}

	function ProcessMove(float DeltaTime, vector NewAccel, eDoubleClickDir DoubleClickMove, rotator DeltaRot)
	{
		
		if( Pawn == None )
		{
			return;
		}

		if (Role == ROLE_Authority)
		{
			// Update ViewPitch for remote clients
			Pawn.SetRemoteViewPitch( Rotation.Pitch );
		}

		Pawn.Acceleration = NewAccel;
	}
	event PlayerTick(float dTime)
	{
		super.PlayerTick(dTime);

		if(CMPawn(Pawn).bFoundEdge)
		{
			if(CMPawn(Pawn).InUpperRange())
			{

			}
		}
	}
	function PlayerMove( float DeltaTime )
	{
		local vector			X,Y,Z, NewAccel;
		local eDoubleClickDir	DoubleClickMove;
		local rotator			OldRotation;
		local bool				bSaveJump;

		if( Pawn == None )
		{
			GotoState('Dead');
		}
		else
		{
			//Pawn.b
			GetAxes(Pawn.Rotation,X,Y,Z);

			// Update acceleration.
			NewAccel = PlayerInput.aForward*X + PlayerInput.aStrafe*Y;
			NewAccel.Z	= 0;
			NewAccel = Pawn.AccelRate * Normal(NewAccel);

			if (IsLocalPlayerController())
			{
				AdjustPlayerWalkingMoveAccel(NewAccel);
			}

			DoubleClickMove = PlayerInput.CheckForDoubleClickMove( DeltaTime/WorldInfo.TimeDilation );

			// Update rotation.
			OldRotation = Rotation;
			UpdateRotation( DeltaTime );


			if( Role < ROLE_Authority ) // then save this move and replicate it
			{
				ReplicateMove(DeltaTime, NewAccel, DoubleClickMove, OldRotation - Rotation);
			}
			else
			{
				ProcessMove(DeltaTime, NewAccel, DoubleClickMove, OldRotation - Rotation);
			}
		}
	}


	event BeginState(Name PreviousStateName)
	{
		DoubleClickDir = DCLICK_None;
		bPressedJump = false;
		GroundPitch = 0;
		if ( Pawn != None )
		{
			Pawn.ShouldCrouch(false);
			if (Pawn.Physics != PHYS_Falling && Pawn.Physics != PHYS_RigidBody) // FIXME HACK!!!
				Pawn.SetPhysics(Pawn.WalkingPhysics);
		}
	}

	event EndState(Name NextStateName)
	{
		GroundPitch = 0;
		if ( Pawn != None )
		{
			Pawn.SetRemoteViewPitch( 0 );
			if ( bDuck == 0 )
			{
				Pawn.ShouldCrouch(false);
			}
		}
	}

Begin:
}
state PlayerSprinting
{
	local float prevAccelRate,prevGroundSpeed;
	function ProcessMove(float DeltaTime, vector NewAccel, eDoubleClickDir DoubleClickMove, rotator DeltaRot)
	{
		
		if( Pawn == None )
		{
			return;
		}

		if (Role == ROLE_Authority)
		{
			// Update ViewPitch for remote clients
			Pawn.SetRemoteViewPitch( Rotation.Pitch );
		}

		Pawn.Acceleration = NewAccel;

	}
	function PlayerMove( float DeltaTime )
	{
		local vector			X,Y,Z, NewAccel;
		local eDoubleClickDir	DoubleClickMove;
		local rotator			OldRotation;
		local bool				bSaveJump;

		if( Pawn == None )
		{
			GotoState('Dead');
		}
		else
		{
			GetAxes(Pawn.Rotation,X,Y,Z);

			// Update acceleration.
			NewAccel = PlayerInput.aForward*X + PlayerInput.aStrafe*Y;
			NewAccel.Z	= 0;
			NewAccel = Pawn.AccelRate * Normal(NewAccel);

			if (IsLocalPlayerController())
			{
				AdjustPlayerWalkingMoveAccel(NewAccel);
			}

			DoubleClickMove = PlayerInput.CheckForDoubleClickMove( DeltaTime/WorldInfo.TimeDilation );

			// Update rotation.
			OldRotation = Rotation;
			UpdateRotation( DeltaTime );

			if( Role < ROLE_Authority ) // then save this move and replicate it
			{
				ReplicateMove(DeltaTime, NewAccel, DoubleClickMove, OldRotation - Rotation);
			}
			else
			{
				ProcessMove(DeltaTime, NewAccel, DoubleClickMove, OldRotation - Rotation);
			}
		}
	}

	event PlayerTick(float dTime)
	{
		super.PlayerTick(dTime);

		if(CMPawn(Pawn).bFoundEdge)
		{
			bestEdgeLocation = CMPawn(Pawn).bestEdgeLocation;

			if(VSize(Pawn.Acceleration) > 0)
			{
				if(CMPawn(Pawn).InLowerRange())
				{
					`log("Going to quickvault");
					GotoState('QuickVault');
				}
				else if(CMPawn(Pawn).InMiddleRange())
				{
					`log("Going to handvault");
					GotoState('HandVault');
				}
				else if(CMPawn(Pawn).InUpperRange())
				{
					`log("Going to highvault");
					GotoState('HighVault');
				}
			}
		}
	}
	event BeginState(name PreviousStateName)
	{
		`log("in state "@GetStateName());
		prevAccelRate=Pawn.AccelRate;
		prevGroundSpeed=Pawn.GroundSpeed;
		Pawn.GroundSpeed = 768;
		Pawn.AccelRate=768;
	}
	event endState(name NextStateName)
	{
		`log("out state"@GetStateName());
		Pawn.GroundSpeed = prevGroundSpeed;
		Pawn.AccelRate = prevAccelRate;
	
	}
}

event PreBeginPlay()
{

	Super.PostBeginPlay();

}

exec function ChimpSlap()
{
	`log("Chimp slap that block mofo");
}
exec function QuickTurnLeft()
{
	if(GetStateName() != 'QuickTurn' && GetStateName() != 'Vault')
	{
		leftTurn=1;
		GoToState('QuickTurn');
	}
}
exec function QuickTurnRight()
{
	if(GetStateName() != 'QuickTurn' && GetStateName() != 'Vault')
	{
		leftTurn=0;
		GoToState('QuickTurn');
	}
}
exec function Sprint()
{
	SprintingSound.Play();
	bStoppedSprint=false;
	//CMPawn(Pawn).Sprint();
	if(GetStateName()!='Vault' && GetStateName()!='MonkeySwing')
	{
		GotoState('PlayerSprinting');
	}

}
exec function StopSprint()
{
	SprintingSound.Stop();
	bStoppedSprint=true;
	if(GetStateName()==('PlayerSprinting'))
	GotoState(Pawn.LandMovementState);
	//CMPawn(Pawn).StopSprint();
	
}
exec function ChargeJump()
{
	CMPawn(Pawn).EndClimbMonkeyBar();
	if(GetStateName() != 'Vault')
	{
	JumpSound.Play();

	bJustJumped=true;

		if(bJumpKeyIsUp)
		{
			CMPawn(Pawn).lastTimeJumpPressed = WorldInfo.TimeSeconds;
			bJumpKeyIsUp=false;
		}


			CMPawn(Pawn).DoJump(true);
	}
	
}
exec function Slide()
{
	CMPawn(Pawn).EndClimbMonkeyBar();

	if(CMPawn(Pawn).bIsGrounded)
	{
		GoToState('PowerSlide');
	}
	else
	{
		if(CMPawn(Pawn).wallJumpEnergy>0 && Pawn.Velocity.Z !=0 && CMPawn(Pawn).tracedSomething && !CMPawn(Pawn).bIsGrounded)
		{
			
				GoToState('WallJump');
		}
	}
}
exec function ReleaseJump()
{
	//Play Jump Sound - Paul
	//JumpSound.Play();
	bJumpKeyIsUp=true;
	CMPawn(Pawn).bChargingJump=false;
}

exec function ShootPositiveCharge()
{
	Pawn.Mesh.PlayAnim('Shoot',0.3f,false,false);
	ShootSound.PitchMultiplier = 0;
	ShootSound.Play();
	if(CMPawn(Pawn).currentTracedMagneticActor!=None)
	{
		CMPawn(Pawn).ShootPositiveCharge();
	}
}
exec function ReleasePositiveCharge()
{
	CMPawn(Pawn).ReleasePositiveCharge();
}
exec function ShootNegativeCharge()
{
	Pawn.Mesh.PlayAnim('Shoot',0.3f,false,false);
	ShootSound.PitchMultiplier = 2;
	ShootSound.Play();
	if(CMPawn(Pawn).currentTracedMagneticActor!=None)
	{
		CMPawn(Pawn).ShootNegativeCharge();
	}
}
exec function ReleaseNegativeCharge()
{
	CMPawn(Pawn).ReleaseNegativeCharge();
}
exec function ToggleCamera()
{
	CMPawn(Pawn).ToggleCamera();
}
exec function PrevCharge()
{
	//Play charge sound
	ChangeChargeSound.Play();
	CMPawn(Pawn).PrevCharge();
}
exec function NextCharge()
{
	//Play charge sound
	ChangeChargeSound.Play();
	CMPawn(Pawn).NextCharge();
}
exec function ShootMagneticCharge()
{

	Pawn.Mesh.PlayAnim('Shoot',0.3f,false,false);
	//Play shoot sound - Paul
	ShootSound.Play();
	if(CMPawn(Pawn).currentTracedMagneticActor!=None)
	{
		CMPawn(Pawn).ShootMagneticCharge();
	}
}
exec function ResetMagnetPosition()
{
	local CMMagneticBox theBox;
	foreach AllActors(class'CMMagneticBox',theBox)
	{
		theBox.resetMagPosition();
	}
}




DefaultProperties
{
	//Sounds - Paul
	Begin Object class=AudioComponent name=SprintingAudioComponent
		SoundCue = SoundCue'Chimp_Magnet.Sound.Run_Cue'
	End Object
	SprintingSound = SprintingAudioComponent

	Begin Object class=AudioComponent name=JumpingAudioComponent
		SoundCue = SoundCue'Chimp_Magnet.Sound.Jump_Cue'
	End Object
	JumpSound = JumpingAudioComponent

	Begin Object class=AudioComponent name=ShootAudioComponent
		SoundCue = SoundCue'Chimp_Magnet.Sound.Shoot_Cue'
	End Object
	ShootSound = ShootAudioComponent

	Begin Object class=AudioComponent name=ChangeChargeAudioComponent
		SoundCue = SoundCue'Chimp_Magnet.Sound.ChangeCharge_Cue'
	End Object
	ChangeChargeSound = ChangeChargeAudioComponent

	bStoppedSprint = true

	InputClass=class'CMPlayerInput'
	slowdown=false
	slowdowntimercount=0

	lastKnownAccelRate=0
	lastKnownAcceleration=(X=0,Y=0,Z=0)
	lastKnownVelocity=(X=0,Y=0,Z=0)
}
