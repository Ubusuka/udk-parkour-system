/*=============================================================================
// LadderVolumes, when touched, cause ladder supporting actors to use Phys_Ladder.
// note that underwater ladders won't be waterzones (no breathing problems)
// Copyright 1998-2012 Epic Games, Inc. All Rights Reserved.
============================================================================= */

class CMMonkeyBarVolume extends Actor placeable;



var(Custom) StaticMeshComponent StaticMeshComponent;
var() editconst const CylinderComponent	CylinderComponent;


event PostBeginPlay()
{
	super.PostBeginPlay();
	SetCollisionType(COLLIDE_TouchAll);
}

event Touch(Actor other,PrimitiveComponent otherComp,Vector HotLoc,Vector HitNorm)
{
	local vector ZAxis;
	ZAxis.Z=1;
	
	if(other.IsA('CMPawn'))
	{
		
		CMPawn(other).magnetInteractiveSide=Self;

		if(Base!=None)
		{
			//`log("YES!!");
			
		}
		
		`log("DOT: "@vector(Rotation) dot vector(CMPawn(other).Rotation));
		`log("DOT2: "@vector(Rotation) dot ZAxis);
			if(vector(Rotation) dot ZAxis == 0  && 
				vector(CMPawn(other).Rotation) dot vector(Rotation) < 0
				&&CMPawn(other).bMagnetLadderInfront)
			{
				//`log("LADDER");
				CMPawn(other).ClimbMagnetLadder(Self);
			}
		     if(vector(Rotation) dot ZAxis == -1 && CMPawn(other).bMonkeyBarOverhead)
			{
				`log("MONKEY");
				CMPawn(other).SwingMonkeyBars(Self);
			}

	}
	
}
event UnTouch(Actor Other)
{
	local vector ZAxis;
	ZAxis.Z=1;
   if(Other.IsA('CMPawn'))
	{

			if(vector(Rotation) dot zAxis == 0)
			{
				if(CMPawn(Other).bMagnetLadderInfront==false)
				{
					//Base.SetCollision(true,true,true);
				//`log("Its a ladder");
					CMPawn(Other).StopMagnetLadder();
				}
			}
			else if(vector(Rotation) dot ZAxis == -1)
			{
				if(CMPawn(Other).bMonkeyBarOverhead==false)
				{
					`log("YEP");
					Base.SetCollision(true,true,true);
					//`log("Its monkey Bars");
					CMPawn(Other).EndClimbMonkeyBar();
				}
			}
		

	}
}
event Tick(float dTime)
{
	
}

defaultproperties
{
	Begin Object Class=SpriteComponent Name=Sprite
		Sprite=Texture2D'EditorResources.S_Trigger'
		HiddenGame=true
		AlwaysLoadOnClient=False
		AlwaysLoadOnServer=False
		SpriteCategoryName="Triggers"
	End Object
	Components.Add(Sprite)


	 Begin Object Class=StaticMeshComponent Name=StaticMeshComponent0
	HiddenEditor=false
	HiddenGame=true
	BlockNonZeroExtent=true
	BlockZeroExtent=true
		End Object
	CollisionComponent=StaticMeshComponent0
	StaticMeshComponent=StaticMeshComponent0

	Components.Add(StaticMeshComponent0)

	//bHidden=true
	bCollideActors=true
	bProjTarget=true
	bStatic=false
	bNoDelete=true
	bBlockActors=false
	bCollideActors=true

}
